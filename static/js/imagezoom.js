const images = Array.from(document.querySelectorAll("img"));
images.forEach(img => {
  if (img.src.includes("#zoomable")) {
    mediumZoom(img, {
      margin: 0, /* The space outside the zoomed image */
      scrollOffset: 40, /* The number of pixels to scroll to close the zoom */
      background: "#303030",
      container: null, /* The viewport to render the zoom in */
      template: null /* The template element to display on zoom */
    });
  }
});

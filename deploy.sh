#!/usr/bin/env bash
set -euo pipefail

# remove existing directory
rm -rf public/

# build website
HUGO_ENV="production" hugo --gc --minify || exit 1

# NOTE: this will push to the digital ocean droplet that hosts the site.
# You will need to have keys there for this to work.
rsync -rv --delete --exclude=".DS_Store" public/ root@mergetb.org:/var/www/next.mergetb.org/

module gitlab.com/mergetb/website

go 1.22.0

require (
	github.com/google/docsy v0.11.0 // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
)

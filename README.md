# mergetb/website

**website** contains the website for Merge v1.0+ and is currently
located at <https://next.mergetb.org>.

This website uses the [Docsy](https://github.com/google/docsy) Hugo
theme.

## Building

1. install software dependencies (Hugo *extended*, git, node)
    * macOS/MacPorts:
      ```
      sudo port install git nodejs17
      sudo port install hugo +extended
      ```
    * Fedora:
      ```
      sudo dnf install hugo
      ```
2. clone this repository:
   ```
   git clone git@gitlab.com:mergetb/website.git
   ```
3. install website dependencies:
   ```
   cd website
   git submodule update --recursive --init
   npm install .
   ```
4. install docsy's dependencies:
   ```
   cd themes/docsy
   npm install .
   ```
5. to build, run `hugo` and browse the output `public/` folder
6. to run locally, run `hugo serve -D` and browse to <http://localhost:1313>

## Deploying

If the content compiles successfully on your local machine, we should
first do a test deployment through CI/CD and GitLab Pages, then push it
to production.

### Deploying to Staging (mergetb.gitlab.io/website)

- All pushed commits to any branch gets built through CI/CD.
  You should check to make sure the build is successful before
  opening an MR.

- Additionally, all pushed commits to the primary branch
  gets automatically deployed to <https://mergetb.gitlab.io/website>.

  Because the URL is different from Production, some things won't work
  properly or return the "correct" results:

    * Algolia's DocSearch
    * (and maybe others)

### Deploying to Production

To deploy to <https://www.mergetb.org>, run `./deploy.sh`.

## Notes

- to convert `.eps` to `.png` with transparency, use something like:
  ```
  convert -background transparent -colorspace sRGB -density 1200 VsoE_ISI_Form_Horiz_OneColor-01.eps -transparent "#ffffff" -resize 600x isi.png
  ```

## License

- Docsy Hugo theme: Apache License 2.0 (`Apache-2.0`)
- MergeTB content: Creative Commons Attribution 4.0 International (`CC-BY-4.0`)

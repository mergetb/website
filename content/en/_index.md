---
title: MergeTB Documentation
---

{{< blocks/cover title="The Merge Testbed Platform" subtitle="Conduct experimental research on networked systems." image_anchor="top" height="" color="primary">}}
<br />
<div class="mx-auto">
	<a class="btn btn-lg btn-secondary mr-3 mb-4" href="{{< relref "/docs" >}}">
		Learn More <i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
	<a class="btn btn-lg btn-secondary mr-3 mb-4" href="https://gitlab.com/mergetb">
		Releases <i class="fab fa-gitlab ml-2 "></i>
	</a>
</div>
{{< /blocks/cover >}}

{{% blocks/lead color="white" %}}
  [Merge]({{< relref "/docs" >}}) is a network testbed platform.

  **Build** the sytems of interest with bare-metal and virtual machines.

  **Interconnect** those machines through high-fidelity emulated networks.

  **Run** experiments that shape tomorrow's systems.
{{% /blocks/lead %}}

{{% blocks/section color="dark" type="row" %}}

  {{% blocks/feature icon="fa-lightbulb" title="Create Experiments" %}}
  Use Merge to explore new ideas in interconnected systems.

  Develop experiment models that rigorously define the domain of
  applicability. A code-driven approach to experiment developemnt with
  powerful tools for compilation, static analysis and model fragment
  generation.
  {{% /blocks/feature %}}

  {{% blocks/feature icon="fa fa-bolt" title="Evaluate Systems" %}}
  Create dynamic and challenging environments for systems to operate in
  to test resilience and robustness.

  Describe the environment in which your experiment exists using emulated
  networks and computers, simulated physical systems and agents that model
  human behavior.
  {{% /blocks/feature %}}

  {{% blocks/feature icon="fa fa-server" title="Build Testbed Facilities" %}}
  Use Merge to build testbeds with unique capabilites that open new
  doors in experimentation.

  Run your experiments on one of our testbed facilities, join our
  testbed ecosystem as a resource provider, or build your own
  distributed system of testbeds using Merge technology.
  MergeTB is a modular platform for building testbed ecosystems.
  {{% /blocks/feature %}}
{{% /blocks/section %}}

{{< blocks/section color="white">}}
<div class="container-fluid">
  <div class="row">
    <div class="col-12 text-center">
      <img src="{{< siteurl >}}img/isi.png"   class="brand-logo px-0 py-2" />
      <img src="{{< siteurl >}}img/darpa.png" class="brand-logo px-0 py-2" />
      <img src="{{< siteurl >}}img/arpae.png" class="brand-logo px-0 py-2" />
    </div>
  </div>
</div>
{{< /blocks/section >}}

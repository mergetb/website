---
title: "Documentation"
linkTitle: "Documentation"
weight: 20
menu:
  main:
    weight: 20
---

Welcome to the Merge documentation. To get a basic understanding of what Merge is, head to
[concepts](/docs/concepts). To learn more about using Merge for creating networked-system
experiments, head to [experimentation](/docs/experimentation). For information of operating a Merge
portal, head to [portal operations](/docs/portal/operation). To understand more about testbed facility
operations, head to [facility operations](/docs/facility/operate). To get acclimated with development in
Merge, head to [development](/docs/development).

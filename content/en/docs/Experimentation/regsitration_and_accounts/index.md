---
title: "Accounts and Account Registration"
linkTitle: "Accounts and Account Registration"
weight: 4
description: >
    Description of user accounts on Merge-based testbeds and techniques for account registration.
---
### Accounts and Account Registration

#### Overview

In order to use a Merge-based testbed, an account is required. A user account organizes personal projects, experiments, and organizations. An account also allows you to join other projects or organization. The account is a mixture of personal user information (name, location, etc) and authentication information (email, username, password, Open Id account). On Merge-based testbeds, these classes of information are kept separate as the authentication information is used for Merge’s builtin Identity Management System and is used primarily for ensuring users have access to resources to which they have been granted and integrating with other 3rd party Identity management systems. Personal information (institutional affiliation, account type, etc) is used for user social networking and keeping track of general testbed usage.

Account information:

* Identity (for authentication)
  * username - must be unique across the system. The primary ID of an account
  * password - authentication data. Used to login via a GUI or command line
  * OIDC - third party authentication (optional and per-portal configurable)
  * email address - for account recovery and verification
* User Information
  * Full Name
  * Institutional Affiliation (if any)
  * User Category (e.g. `student`, `researcher` etc)
  * Country
  * State (if country is United States)

#### Account Registration

There are a two ways to register for an account on a Merge-based testbed: via a GUI (graphical user interface) or on the command line.

Once the registration is complete, a testbed administrator (or Organization Maintainer) needs to approve and activate the account before if can be used. During registration
an Organization membership may be requested. If so an Organization Maintainer can approve and activate the account.

##### Command Line Registration

The `mrg` command line utility allows a user to interact with the testbed. The registration command does not require authentication (of course) and allows an arbitrary user to request a new account. 
The usage is:

```
Usage:
  mrg register <username> <email> <full name> <institution> <category> <country> [flags]

Flags:
  -h, --help                       help for register
  -o, --organization string        request organization membership
  -r, --organization_role string   request organization role (one of Member,Maintainer,Creator) (default "Member")
  -p, --passwd string              user password
      --usstate string             user US state
```

The username, email, full name, institution, user category and country is required. If the country is the United States, then the US state of residency is also required. The known institutions and user categories can be listed from the command line without an account. To list institutions: mrg list portal institutions. To list user categories: mrg list portal user-categories. Note that these lists are configured for each testbed so may differ across different Merge testbed deployments.

As part of registration, a user can also request membership in a given existing Organization on the testbed. (This is generally not used unless you are creating a new account within the organization on the testbed. Like a student registering for an account in a class or a researcher registering for an account on a project.) If the membership is approved, the Organization maintainers have full control of the account. 

Example:
```
mrg register murphy murphy@example.net 'Murphy Goodfellow' ISI Researcher 'United States' --usstate California' -p 98021jhodqiuwda#@#dsasd
```

And requesting organization membership:
```
mrg register murphy murphy@example.net 'Murphy Goodfellow' ISI Researcher 'United States' --usstate California' -p 98021jhodqiuwda#@#dsasd -o USC101
```

##### Graphical Registration

The account registration link is found on the login page of the GUI. It is under the `Username` and `Password` fields of the login form. The registration flow starts with the `Identity` account information, then the `User` information. If the portal is configured to support 3rd party Identity servers, they will be shown along side the `Identity` account information. 

Example with standard username/password account and 3rd party CILOGIN account. 

![](ident-account-info.png)

##### Name and Password

Supplying a username and password is always supported.  If you choose to not use a 3rd party service or the portal is not configured to support one, enter the required fields `username`, `email address` and `password`. The portal may be configured to also have optional fields. Fill the optional fields or not. Click `Sign Up` and you will be taken to the `User Account` page. The `username` and `email address` fields are filled out for you. All other fields are required.

![](userpass-useraccount.png)

Once filled out, push `Register` and the account is created. 

##### Third Party (OIDC)

If the portal is configured to support 3rd party login, click the button for the Identity server you wish to use. (In this example, it is [CILOGON](https://cilogin.net). When using a 3rd party identity server the `username` and `email address` fields are still required. The 3rd party identity server may or may not supply these fields. If they do not, then a page requesting the missing fields is shown. Fill out the missing fields and push the submit button. You will then be taken to the `User Account` page as above. Fill this out and push `Register`. The account is created. 

{{% alert title="Note" color="warning" %}}
Note that passwords are required for some interactions with a Merge portal. Namely logging into the command line utility `mrg`. This is required for `ssh` access to any [XDC](../xdc.md) you create. If you use a 3rd party identity service to login, you will need to set a password in the system to use `mrg`. This is done by "resetting" the password once logged in. Login and click the user menu on the upper right. Click `Update Identity Profile`. On the next page change your password from unset to a strong password value. 

Once your password is set you can login to the GUI via username and password or the 3rd party identity service. 

![](update-ip.png)

{{% /alert %}}


---
title: Research Papers
description: Research Papers about Merge.
weight: 1
---

{{% blocks/section type="section" %}}

# [Projects](../) / Research Papers

This page contains a list of research papers that have been written
about the Merge testbed platform and its various components.

{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "Collins24a" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "238258" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1109/infcomw.2019.8845249" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.48550/arxiv.1810.08260" >}}
{{% /blocks/section %}}

---
title: Searchlight
description: >
    Papers, software, and other artifacts in support of the DARPA
    Searchlight program on MergeTB.
type: docs
weight: 5
---

The Test and Evaluation (T&E) team, consisting of researchers at Sandia
National Laboratories (SNL) and [USC/ISI](https://www.isi.edu),
developed tools and methodology on Merge testbeds to evaluate performer
technologies for the
[DARPA Searchlight](https://www.darpa.mil/program/searchlight)
program.

We have released a variety of [datasets](#datasets) and
[software](#software), and published [research papers](#papers).

Questions, comments, and bugfixes should be sent to the corresponding
author(s) of the artifact in question, or you can email <calvin@isi.edu>
and get a pointer in the right direction.

Distribution Statement "A" (Approved for Public Release, Distribution
Unlimited). <br>
This research was developed with funding from the Defense
Advanced Research Projects Agency (DARPA). The views, opinions and/or
findings expressed are those of the author and should not be interpreted
as representing the official views or policies of the Department of
Defense or the U.S. Government.

## Datasets

- [The DARPA SEARCHLIGHT Dataset of Application Network Traffic](dataset):
  a dataset containing ~2000 systematically conducted experiments and
  resulting packet captures with contemporary video streaming, video
  teleconferencing, and cloud-based document editing applications.

## Software

### Experimentation on Network Emulation Testbeds

- [Merge Testbed Platform](https://mergetb.org/)
- [minimega-based network experimentation](https://gitlab.com/mergetb/exptools/minimega-cset-2021) (GitLab):
  experiment topologies and application traffic described in the 2021
  CSET paper "[Case Studies in Experiment Design on a minimega Based
  Network Emulation Testbed](#10.1145/3474718.3474730)".

### Traffic Generators

- [Video-on-Demand Streaming](https://gitlab.com/mergetb/exptools/video-streaming) (GitLab)
- [Video Teleconference](https://gitlab.com/mergetb/exptools/video-teleconference) (GitLab)

## Research Papers

### 2022

- {{< bibentrysmall "10.1145/3565476.3569097" >}}
- {{< bibentrysmall "10.1145/3546096.3546103" >}}
- {{< bibentrysmall "10.1145/3546096.3546107" >}}
- {{< bibentrysmall "10.1109/EuroSPW55150.2022.00046" >}}

### 2021

- {{< bibentrysmall "10.1145/3474718.3474730" >}}
- {{< bibentrysmall "10.1145/3474718.3474721" >}}
